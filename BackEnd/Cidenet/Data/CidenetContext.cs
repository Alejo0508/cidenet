﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Cidenet.Model;

namespace Cidenet.Data
{
    public class CidenetContext : DbContext
    {
        public CidenetContext (DbContextOptions<CidenetContext> options)
            : base(options)
        {
        }

        public DbSet<Cidenet.Model.Empleados> Empleados { get; set; } = default!;


    }
}
