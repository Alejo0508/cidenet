﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Cidenet.Model
{
    public class Empleados
    {
        [Key]
        public int UsuarioId { get; set; }
        public string PrimerApellido { get; set; }
        public string? SegundoApellido { get; set; }
        public string? PrimerNombre { get; set; }
        public string? OtrosNombres { get; set; }
        public string PaisEmpleo { get; set; }
        public string TipoIdentificacion { get; set; }
        public string NumeroIdentificacion { get; set; }
        public string CorreoElectronico { get; set; }
        public DateTime FechaIngreso { get; set; }
        public string Area { get; set; }
        public Boolean Estado { get; set; }
        public DateTime? FechaRegistro { get; set; }
        public DateTime? FechaEdicion { get; set; }
    }
}
