﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Cidenet.Data;
using Cidenet.Model;

namespace Cidenet.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmpleadosController : ControllerBase
    {
        private readonly CidenetContext _context;

        public EmpleadosController(CidenetContext context)
        {
            _context = context;
        }

        // GET: trae todos los empleados registrados
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Empleados>>> GetEmpleados()
        {
          if (_context.Empleados == null)
          {
              return NotFound();
          }
            return await _context.Empleados.ToListAsync();
        }

        // GET: trae enmpleados por id
        [HttpGet("{UsuarioId}")]
        public async Task<ActionResult<Empleados>> GetEmpleados(int UsuarioId)
        {
          if (_context.Empleados == null)
          {
              return NotFound();
          }

           var empleados = await _context.Empleados.FindAsync(UsuarioId);

            if (empleados == null)
            {
                return NotFound();
            }

            return empleados;
        }

        // buscar usuarios por apellido
        [HttpGet("/api/Empleados/Identificacion/{NumeroIdentificacion}")]
        public async Task<ActionResult<Empleados>> GetEmpleadosPorApellido(String NumeroIdentificacion)
        {
            if (_context.Empleados == null)
            {
                return NotFound();
            }
            int cont=0;

            var empleados2 = await _context.Empleados
                .Where(x => x.NumeroIdentificacion.Equals(NumeroIdentificacion))
                .Select(c => new Empleados()
                {
                    UsuarioId = c.UsuarioId,
                    PrimerApellido = c.PrimerApellido,
                    SegundoApellido =  c.SegundoApellido,
                    PrimerNombre = c.PrimerNombre,
                    OtrosNombres = c.OtrosNombres,
                    PaisEmpleo = c.PaisEmpleo,
                    TipoIdentificacion = c.TipoIdentificacion,
                    NumeroIdentificacion = c.NumeroIdentificacion,
                    CorreoElectronico = c.CorreoElectronico,
                    FechaIngreso = c.FechaIngreso,
                    Area = c.Area,

                }).FirstOrDefaultAsync();

            if (empleados2 == null)
            {
                cont = 1;
                return empleados2;
            }

            return empleados2;
        }

        // PUT: actualiza los empleados

        [HttpPut("{id}")]
        public async Task<IActionResult> PutEmpleados(int id, Empleados empleados)
        {
            if (id != empleados.UsuarioId)
            {
                return BadRequest();
            }

            _context.Entry(empleados).State = EntityState.Modified;

            try
            {              
                empleados.FechaEdicion = DateTime.Now;
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmpleadosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetEmpleados", new { id = empleados.UsuarioId });
        }

        // POST: agrega los empleados

        [HttpPost]
        public async Task<ActionResult<Empleados>> PostEmpleados(Empleados empleados)
        {
          if (_context.Empleados == null)
          {
              return Problem("Entity set 'CidenetContext.Empleados'  is null.");
          }
            empleados.FechaRegistro = DateTime.Now;
            _context.Empleados.Add(empleados);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEmpleados", new { id = empleados.UsuarioId });
        }

        // DELETE: elimina los empleados

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmpleados(int id)
        {
            if (_context.Empleados == null)
            {
                return NotFound();
            }
            var empleados = await _context.Empleados.FindAsync(id);
            if (empleados == null)
            {
                return NotFound();
            }

            _context.Empleados.Remove(empleados);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool EmpleadosExists(int id)
        {
            return (_context.Empleados?.Any(e => e.UsuarioId == id)).GetValueOrDefault();
        }
    }
}
