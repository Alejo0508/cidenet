import { Injectable, Output, EventEmitter } from '@angular/core'

@Injectable()
export class datosEmpleado {

  empleado:any;

  @Output() change: EventEmitter<boolean> = new EventEmitter();

  DatosEmpleado(empleado) {
    this.empleado = empleado
  }

}
