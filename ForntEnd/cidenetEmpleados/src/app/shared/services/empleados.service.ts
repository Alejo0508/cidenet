import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class EmpleadosService {

  constructor(private http: HttpClient) { }

/*   busca todos los empleados */
  getBuscarEmpleados<T>(): Observable<T> {

    return this.http.get('/api/Empleados').pipe(map((response) => response as T));

  }


  /*   busca por cedula */
  getBuscarEmpleadosidentificacion<T>(NumeroIdentificacion): Observable<T> {

    return this.http.get('/api/Empleados/Identificacion/'+NumeroIdentificacion).pipe(map((response) => response as T));

  }

 /*  Elimina un empleado */
  eliminarEmpleados<T>(id): Observable<T> {
    return this.http.delete('/api/Empleados/'+id).pipe(map((response) => response as T));

  }

/* registrar empleado */
  postEmpleado(Empleados): Observable<any>{

    const headers =  { 'content-type': 'application/json'};
    const body = JSON.stringify(Empleados);

  return this.http.post('/api/Empleados', body, {'headers': headers});

  }

    putEmpelados(id, Empleados): Observable<any> {

      const headers =  { 'content-type': 'application/json'};
      const body = JSON.stringify(Empleados);
   
    return this.http.put('/api/Empleados/'+id, body, {'headers': headers});
  
    }


  getCarpoolingReservas<T>(email,inscribir): Observable<T> {

    return this.http.get('http://localhost:3000/infoReservasCarpooler?email='+email+'&inscribir='+inscribir).pipe(map((response) => response as T));

  }


  putCupos(cupos,email){

    console.log("cupos service",cupos)
    console.log("email service",email)

    let totalCuposCarpooler = {
      cupos,
      email
    }

    const headers =  { 'content-type': 'application/json'};
    const body = JSON.stringify(totalCuposCarpooler);

  return this.http.put('http://localhost:3000/actualizarCupos', body, {'headers': headers});



  }


}