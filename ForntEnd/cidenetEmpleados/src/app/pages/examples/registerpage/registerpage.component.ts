import { Component, OnInit, OnDestroy, HostListener } from "@angular/core";
import { EmpleadosService } from 'src/app/shared/services/empleados.service'
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { datosEmpleado } from 'src/app/shared/services/datosEmpleado.service'
import { Injectable } from '@angular/core';

@Component({
  selector: "app-registerpage",
  templateUrl: "registerpage.component.html"
})

@Injectable()
export class RegisterpageComponent implements OnInit, OnDestroy {
  isCollapsed = true;
  focus;
  focus1;
  focus2;


  PrimerApellido: any;
  SegundoApellido: any;
  PrimerNombre: any;
  OtrosNombres: any;
  PaisEmpleo = null;
  CorreoElectronico: any;
  TipoIdentificacion = null;
  NumeroIdentificacion: any;
  FechaIngreso: any;
  FechaRegistro:any;
  Area = null;
  respuesta: any;
  alert;

  constructor(private service: EmpleadosService, private router: Router, private empleado: datosEmpleado) { }

  @HostListener("document:mousemove", ["$event"])
  onMouseMove(e) {

    var squares1 = document.getElementById("square1");
    var squares2 = document.getElementById("square2");
    var squares3 = document.getElementById("square3");
    var squares4 = document.getElementById("square4");
    var squares5 = document.getElementById("square5");
    var squares6 = document.getElementById("square6");
    var squares7 = document.getElementById("square7");
    var squares8 = document.getElementById("square8");

    var posX = e.clientX - window.innerWidth / 2;
    var posY = e.clientY - window.innerWidth / 6;

    squares1.style.transform =
      "perspective(500px) rotateY(" +
      posX * 0.05 +
      "deg) rotateX(" +
      posY * -0.05 +
      "deg)";
    squares2.style.transform =
      "perspective(500px) rotateY(" +
      posX * 0.05 +
      "deg) rotateX(" +
      posY * -0.05 +
      "deg)";
    squares3.style.transform =
      "perspective(500px) rotateY(" +
      posX * 0.05 +
      "deg) rotateX(" +
      posY * -0.05 +
      "deg)";
    squares4.style.transform =
      "perspective(500px) rotateY(" +
      posX * 0.05 +
      "deg) rotateX(" +
      posY * -0.05 +
      "deg)";
    squares5.style.transform =
      "perspective(500px) rotateY(" +
      posX * 0.05 +
      "deg) rotateX(" +
      posY * -0.05 +
      "deg)";
    squares6.style.transform =
      "perspective(500px) rotateY(" +
      posX * 0.05 +
      "deg) rotateX(" +
      posY * -0.05 +
      "deg)";
    squares7.style.transform =
      "perspective(500px) rotateY(" +
      posX * 0.02 +
      "deg) rotateX(" +
      posY * -0.02 +
      "deg)";
    squares8.style.transform =
      "perspective(500px) rotateY(" +
      posX * 0.02 +
      "deg) rotateX(" +
      posY * -0.02 +
      "deg)";
  }

  ngOnInit() {

    let registrar = document.getElementById("registrar");
    let actualizar = document.getElementById("actualizar");
    let fechaIngreso = document.getElementById("datepicker");
    var empleado = this.empleado.empleado;

    if (this.empleado.empleado) {
      registrar.style.display = 'none';
      actualizar.style.display = 'block';
      fechaIngreso.style.display = 'none'

      this.PrimerApellido = empleado.primerApellido;
      this.SegundoApellido = empleado.segundoApellido;
      this.PrimerNombre = empleado.primerNombre;
      this.OtrosNombres = empleado.otrosNombres;
      this.PaisEmpleo = empleado.paisEmpleo;
      this.TipoIdentificacion = empleado.tipoIdentificacion;
      this.NumeroIdentificacion = empleado.numeroIdentificacion;
      this.CorreoElectronico = empleado.correoElectronico;
      this.FechaIngreso = empleado.fechaIngreso;
      this.FechaRegistro = empleado.fechaRegistro;
      this.Area = empleado.area;
      
    } else {
      registrar.style.display = 'block';
      actualizar.style.display = 'none';
    }

    console.log(empleado)



    var body = document.getElementsByTagName("body")[0];
    body.classList.add("register-page");

    this.onMouseMove(event);
  }
  ngOnDestroy() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.remove("register-page");
  }


  CorreoDominio() {
    let dominio;
    let pais = this.PaisEmpleo;

    if (pais == "Colombia" && this.PrimerApellido && this.PrimerNombre) {
      dominio = "@cidenet.com.co"
      this.CorreoElectronico = this.PrimerNombre + "." + this.PrimerApellido + dominio;
    } else {

      if (pais == "Estados unidos" && this.PrimerApellido && this.PrimerNombre) {
        dominio = "@cidenet.com.eu"
        this.CorreoElectronico = this.PrimerNombre + "." + this.PrimerApellido + dominio;

      } else {
        this.CorreoElectronico = ""
      }

    }
  }

  Regresar() {

    this.PrimerApellido = "";
    this.SegundoApellido = "";
    this.PrimerNombre = "";
    this.OtrosNombres = "";
    this.PaisEmpleo = "";
    this.CorreoElectronico = "";
    this.TipoIdentificacion = "";
    this.NumeroIdentificacion = "";
    this.FechaIngreso = "";
    this.Area = "";

    this.empleado.empleado = ""

    this.router.navigate(['/home']);
  }

  Registrar() {

    const formatDate = (current_datetime) => {
      let formatted_date = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getDate() + " " + current_datetime.getHours() + ":" + current_datetime.getMinutes() + ":" + current_datetime.getSeconds();
      return formatted_date;
    }

    let Empleados = {

      PrimerApellido: this.PrimerApellido,
      SegundoApellido: this.SegundoApellido,
      PrimerNombre: this.PrimerNombre,
      OtrosNombres: this.OtrosNombres,
      PaisEmpleo: this.PaisEmpleo,
      CorreoElectronico: this.CorreoElectronico,
      TipoIdentificacion: this.TipoIdentificacion,
      NumeroIdentificacion: this.NumeroIdentificacion,
      FechaIngreso: formatDate(this.FechaIngreso),
      Area: this.Area
    }

    let alert = document.getElementById("alert");
    let alert2 = document.getElementById("alert2");

    this.service.getBuscarEmpleadosidentificacion(this.NumeroIdentificacion).subscribe(data => {
      console.log(data)

      if(data){
        alert2.style.display = 'block';
      }else{
        alert.style.display = 'block';

        this.service.postEmpleado(Empleados).subscribe(data => {
          this.respuesta = data;
          if (data) {
            alert.style.display = 'block';
    
            setTimeout(() => {
              this.router.navigate(['/home']);
            }, 1000)
          }
        }); 
      }

    });

    
  }


  Actualizar() {

    const formatDate = (current_datetime) => {
      let formatted_date = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getDate() + " " + current_datetime.getHours() + ":" + current_datetime.getMinutes() + ":" + current_datetime.getSeconds();
      return formatted_date;
    }

    var empleado = this.empleado.empleado;

    let Empleados = {
      usuarioId: empleado.usuarioId,
      PrimerApellido: this.PrimerApellido,
      SegundoApellido: this.SegundoApellido,
      PrimerNombre: this.PrimerNombre,
      OtrosNombres: this.OtrosNombres,
      PaisEmpleo: this.PaisEmpleo,
      FechaIngreso: this.FechaIngreso,
      FechaRegistro: this.FechaRegistro,
      CorreoElectronico: this.CorreoElectronico,
      TipoIdentificacion: this.TipoIdentificacion,
      NumeroIdentificacion: this.NumeroIdentificacion,
      Area: this.Area
    }

    let alert = document.getElementById("alert");

console.log(Empleados)
     this.service.putEmpelados(empleado.usuarioId, Empleados).subscribe(data => {
      this.respuesta = data;
      if (data) {
        alert.style.display = 'block';

        setTimeout(() => {
          this.router.navigate(['/home']);
        }, 1000)
      }
    }); 
  }


}
