import { Component, OnInit, OnDestroy } from "@angular/core";
import noUiSlider from "nouislider";
import { EmpleadosService } from 'src/app/shared/services/empleados.service'
import { datosEmpleado } from 'src/app/shared/services/datosEmpleado.service'
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';



@Component({
  selector: "app-index",
  templateUrl: "index.component.html"
})
export class IndexComponent implements OnInit, OnDestroy {
  isCollapsed = true;
  focus;
  focus1;
  focus2;
  date = new Date();
  pagination = 3;
  pagination1 = 1;
  Empleados: any;
  filtroBusqueda: any;

  constructor(private service: EmpleadosService, private router: Router, private empleado: datosEmpleado) { }

  scrollToDownload(element: any) {
    element.scrollIntoView({ behavior: "smooth" });
  }

  ngOnInit() {

    this.service.getBuscarEmpleados().subscribe(data => {
      this.Empleados = data;
    });

    var body = document.getElementsByTagName("body")[0];
    body.classList.add("index-page");

    var slider = document.getElementById("sliderRegular");

    noUiSlider.create(slider, {
      start: 40,
      connect: false,
      range: {
        min: 0,
        max: 100
      }
    });

    var slider2 = document.getElementById("sliderDouble");

    noUiSlider.create(slider2, {
      start: [20, 60],
      connect: true,
      range: {
        min: 0,
        max: 100
      }
    });
  }


  Buscar() {
    if (this.filtroBusqueda == "") {
      this.ngOnInit();
    } else {
      this.Empleados = this.Empleados.filter(res => {

        let otroNombre;
        if (res.otrosNombres == null) {
          otroNombre = ""
        } else {
          otroNombre = res.otrosNombres
        }
        otroNombre.toString().toLowerCase();
        return res.primerApellido.toString().toLowerCase().match(this.filtroBusqueda.toString().toLowerCase())
          || res.segundoApellido.toString().toLowerCase().match(this.filtroBusqueda.toString().toLowerCase())
          || res.primerNombre.toString().toLowerCase().match(this.filtroBusqueda.toString().toLowerCase())
          || otroNombre.match(this.filtroBusqueda.toString().toLowerCase())
          || res.tipoIdentificacion.toString().toLowerCase().match(this.filtroBusqueda.toString().toLowerCase())
          || res.numeroIdentificacion.toString().toLowerCase().match(this.filtroBusqueda.toString().toLowerCase())
          || res.paisEmpleo.toString().toLowerCase().match(this.filtroBusqueda.toString().toLowerCase())
          || res.correoElectronico.toString().toLowerCase().match(this.filtroBusqueda.toString().toLowerCase())
      })
    }

  }

  Eliminar(usuario: any) {
    this.service.eliminarEmpleados(usuario.usuarioId).subscribe(data => {
      location.reload();
    });
  }


  Editar(empleadoDatos) {


    this.empleado.DatosEmpleado(empleadoDatos)

    this.router.navigate(['/register']);

  }


  ngOnDestroy() {
    var body = document.getElementsByTagName("body")[0];
    body.classList.remove("index-page");
  }
}
